Scriptname MineFieldTrigger extends ObjectReference Const
{  }

;-- Properties --------------------------------------
Group Main
	Explosion Property MineExplosion Auto Const
EndGroup


;-- Variables ---------------------------------------


;-- Functions ---------------------------------------

Event OnTriggerEnter(ObjectReference akActionRef)
	
	RegisterForAnimationEvent(akActionRef, "FootLeft")
	RegisterForAnimationEvent(akActionRef, "FootRight")

EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (asEventName == "FootLeft" || asEventName == "FootRight")
		if utility.RandomFloat(0.0, 50.0) <= 25.0
			akSource.placeatme(MineExplosion)
		endif
	endif
EndEvent

Event onTriggerLeave(ObjectReference akActionRef)

	UnregisterForAnimationEvent(akActionRef, "FootLeft")
	UnregisterForAnimationEvent(akActionRef, "FootRight")
	
EndEvent