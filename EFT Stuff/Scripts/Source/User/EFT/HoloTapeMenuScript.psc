Scriptname EFT:HoloTapeMenuScript extends Terminal Conditional

EFTMenuInstallerScript property EFTMenuManagerExtender const auto
{Reference to EFTMenuInstallerScript Quest and Script}

Event OnMenuItemRun(int MenuID, ObjectReference akTerminalRef)
    ;getEditLock()
    ; menuID 1: Uninstall EFT
    ; menuID 2: Install Single Menu
    ; menuID 3: Install Multi Menu
    ; menuID 4: Run Menu Fixer
    ; menuID 5: Run Menu Dump
    if (menuID == 1)
        EFTMenuManagerExtender.uninstall_menu()
    elseif (menuID == 2)
        EFTMenuManagerExtender.single_install()
    elseif (menuID == 3)
        EFTMenuManagerExtender.multimenu_install()
    elseif (menuID == 4)
        EFTMenuManagerExtender.holoTapeMenuFixer()
    elseif (menuID == 5)
        EFTMenuManagerExtender.holoTapeMenuDump()
    endif
    ;locked = false
EndEvent
